README:

Sorry, Mike, I could not figure out how to move a repo over from BitBucket to GitHub
On the bright side, you aren't missing much. I wanted to do a whole thing where you could spawn different people in the world and they would build the world based on their priorities. Would have been super cool.
Might still do it. Definitely should've known better than to try and do that at the end of my last semester, though.

You can hit I to spawn a person in the build. They won't do anything, though.

GOAP:
I totally gave up on this. You can see some code commented out on the people controller script from when I started trying GOAP, but I got stuck very quickly. I don't think what I have is worth many, if any points

Behavior Trees:
I got stuck on behavior trees. I downloaded Panda and started using that. I think I actually understood Panda pretty well, as my print statements were always firing at the right times. But I couldn't get the actual
tasks themselves to work. After debugging for like and hour and realizing that this was the last college assignment I'd ever turn in............. I called it quits. For some reason, my people refused to actually move when a 
task that requested them to move was called. I'm not sure what I was doing wrong. All of this can be found in the people controller and panda people scripts.

Thanks again for everything.