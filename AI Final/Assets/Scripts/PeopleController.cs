﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Panda;

public class PeopleController : MonoBehaviour
{
    public GameObject gameController;
    public GameObject terrainSpawn;
    public GameObject townHall;
    public GameObject tree;
    public GameObject rock;
    public GameObject garden;

    public int crops;

    public string carrying;
    public string goal;
    public string[] actions;

    public int probFarm;
    public int probBaby;

    public bool hasJob;

    void Start()
    {
        hasJob = false;

        gameController = GameObject.FindGameObjectWithTag("Game Controller");
        terrainSpawn = GameObject.FindGameObjectWithTag("Spawner");
        townHall = GameObject.FindGameObjectWithTag("Central");
        tree = GameObject.FindGameObjectWithTag("Wood");
        rock = GameObject.FindGameObjectWithTag("Stone");

        //CheckGoal();
        //MakePlan();
    }

    /*private void CheckGoal()
    {
        if (gameController.GetComponent<GameController>().numCrops < gameController.GetComponent<GameController>().numPeople * 5)
        {
            probFarm = 1;
        }
        else
        {
            probFarm = 0;
            probBaby = 1;
        }

        if (probFarm > probBaby)
        {
            goal = "Farm";
        }
        else
        {
            goal = "Baby";
        }
    }

    private void MakePlan()
    {
        if (goal == "Farm")
        {

        }
    }*/

    [Task]
    bool HasJob()
    {
        return hasJob;
    }

    [Task]
    bool EnoughCrops()
    {
        return (gameController.GetComponent<GameController>().numCrops > gameController.GetComponent<GameController>().numPeople * 5);
    }

    [Task]
    bool IsGarden()
    {
        return (gameController.GetComponent<GameController>().numGardens > 0);
    }

    [Task]
    bool EnoughWood()
    {
        return (gameController.GetComponent<GameController>().numWood > 5);
    }

    [Task]
    void ChopTree()
    {
        print("here");
        hasJob = true;
        Vector3.MoveTowards(gameObject.transform.position, tree.transform.position, 5f);
        carrying = "Wood";
    }

    [Task]
    void CollectCrops()
    {
        hasJob = true;
        garden = GameObject.FindGameObjectWithTag("Garden");
        Vector3.MoveTowards(gameObject.transform.position, garden.transform.position, 5f);
        carrying = "Crops";
    }

    [Task]
    void DropOff()
    {
        hasJob = true;
        Vector3.MoveTowards(gameObject.transform.position, townHall.transform.position, 5f);
        if (carrying == "Wood")
        {
            gameController.GetComponent<GameController>().numWood += 1;
        }
        else if (carrying == "Crops")
        {
            gameController.GetComponent<GameController>().numCrops += 1;
        }

        carrying = "";
        hasJob = false;
    }

    [Task]
    void BuildGarden()
    {
        hasJob = true;
        Vector3.MoveTowards(gameObject.transform.position, townHall.transform.position, 5f);
        gameController.GetComponent<GameController>().numWood -= 5;
        Vector3 next;
        terrainSpawn.GetComponent<TerrainSpawn>().SpawnGarden();
        next = terrainSpawn.GetComponent<TerrainSpawn>().forPerson;
        Vector3.MoveTowards(gameObject.transform.position, next, 5f);
        Instantiate(garden, next, Quaternion.identity);
        hasJob = false;
    }
}
