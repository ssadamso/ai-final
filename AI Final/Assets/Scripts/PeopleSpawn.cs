﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleSpawn : MonoBehaviour
{
    public GameObject gameController;

    public GameObject farmer;

    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetKeyUp(KeyCode.I))
        {
            Instantiate(farmer, new Vector3(24, -29, 26), Quaternion.identity);
            gameController.GetComponent<GameController>().numPeople += 1;
        }
    }
}
