﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainSpawn : MonoBehaviour
{
    public GameObject gameController;

    public GameObject terrain;
    public GameObject[,] terrainMap;

    public GameObject townSquare;
    public GameObject tree;
    public GameObject rock;

    public Vector3 forPerson;

    public int spawnX;
    public int spawnY;

    void Start()
    {
        terrainMap = new GameObject[spawnX, spawnY];

        for (int i = 0; i < spawnX; i++)
        {
            for (int j = 0; j < spawnY; j++)
            {
                GameObject curTerrain = Instantiate(terrain, new Vector3(i * 5, -30, j * 5), Quaternion.identity);
                terrainMap[i, j] = curTerrain;
            }
        }

        SpawnNature();
    }

    private void SpawnNature()
    {
        GameObject townHall = Instantiate(townSquare, new Vector3(Mathf.FloorToInt(spawnX / 2) * 5, -29.8f, Mathf.FloorToInt(spawnY / 2) * 5), Quaternion.identity);
        gameController.GetComponent<GameController>().townHall = townHall;
        gameController.GetComponent<GameController>().numHouses += 1;
        terrainMap[Mathf.FloorToInt(spawnX / 2), Mathf.FloorToInt(spawnY / 2)].tag = "Taken";

        int newX = 0;
        int newY = 0;

        bool treePlaced = false;
        bool rockPlaced = false;

        while(treePlaced == false)
        {
            newX = Random.Range(0, spawnX);
            newY = Random.Range(0, spawnY);

            if (terrainMap[newX, newY].tag == "Free")
            {
                Instantiate(tree, new Vector3(newX * 5, -29.2f, newY * 5), Quaternion.Euler(-90, 0, 0));
                terrainMap[newX, newY].tag = "Taken";
                treePlaced = true;
            }
        }

        while (rockPlaced == false)
        {
            newX = Random.Range(0, spawnX);
            newY = Random.Range(0, spawnY);

            if (terrainMap[newX, newY].tag == "Free")
            {
                Instantiate(rock, new Vector3(newX * 5, -29, newY * 5), Quaternion.Euler(-90, 0, 0));
                terrainMap[newX, newY].tag = "Taken";
                rockPlaced = true;
            }
        }
    }

    public void SpawnGarden()
    {
        int newX = 0;
        int newY = 0;

        bool gardenPlaced = false;

        while (gardenPlaced == false)
        {
            newX = Random.Range(0, spawnX);
            newY = Random.Range(0, spawnY);

            if (terrainMap[newX, newY].tag == "Free")
            {
                forPerson = new Vector3(newX, -29f, newY);
            }
        }
    }
}
