﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameController : MonoBehaviour
{
    public GameObject townHall;

    public int numCrops;
    public int numWood;
    public int numStone;
    public int numHouses;
    public int numGardens;
    public int numPeople;

    // Start is called before the first frame update
    void Start()
    {
        numWood = 0;
        numCrops = 0;
        numStone = 0;
        numHouses = 0;
        numGardens = 0;
        numPeople = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
